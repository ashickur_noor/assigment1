package com.example.ashickurrahman.assignment1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    Button registerBtn, loginBtn;
    EditText userName,pass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        registerBtn=findViewById(R.id.loginregbtn);
        loginBtn=findViewById(R.id.loginloginbutton);

        userName=findViewById(R.id.loginusernam);
        pass=findViewById(R.id.loginpassword);

        Intent intent = getIntent();
        String intentUserName = intent.getStringExtra("username");
        String intentPassword = intent.getStringExtra("password");

        userName.setText(intentUserName);
        pass.setText(intentPassword);

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginIntent=new Intent(LoginActivity.this,MainActivity.class);
                loginIntent.putExtra("username",userName.getText().toString());
                loginIntent.putExtra("password",pass.getText().toString());

                startActivity(loginIntent);
            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });




    }

    private void login() {
        if(userName.getText().toString().equals(""))
        {
            Toast.makeText(this, "Please Type A User Name", Toast.LENGTH_SHORT).show();
            userName.requestFocus();
            return ;
        }
        if(pass.getText().toString().equals(""))
        {
            Toast.makeText(this, "Please Type A Password", Toast.LENGTH_SHORT).show();
            pass.requestFocus();
            return ;
        }

        boolean check=checkDB();

        if(check)
        {
            Toast.makeText(this, "Loged In", Toast.LENGTH_SHORT).show();
            Intent chatIntent = new Intent(LoginActivity.this,ChatActivity.class);
            startActivity (chatIntent);
        }
        else
        {
            Toast.makeText(this, "Wrong Details", Toast.LENGTH_SHORT).show();
        }



    }

    private boolean checkDB() {
        SharedPreferences pref=getApplicationContext().getSharedPreferences(MainActivity.PREF_NAME,0);
        String dbUserName=pref.getString(MainActivity.USER_NAME_PREF,"");
        String dbUserPass=pref.getString(MainActivity.PASS_PREF,"");

        if(userName.getText().toString().equals(dbUserName) && pass.getText().toString().equals(dbUserPass))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
