package com.example.ashickurrahman.assignment1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    static final String PREF_NAME = "assignment1";
    static final String USER_NAME_PREF = "username_assignment1";
    static final String EMAIL_PREF = "email_assignment1";
    static final String PASS_PREF = "pass_assignment1";



    Button registerBtn, loginBtn;
    EditText userName,email,pass,confirmPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        registerBtn=findViewById(R.id.registerbutton);
        loginBtn=findViewById(R.id.loginbutton);

        userName=findViewById(R.id.usernamefield);
        email=findViewById(R.id.emailfield);
        pass=findViewById(R.id.passfiled);
        confirmPass=findViewById(R.id.confirmpassfield);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gotoLoginActivity = new Intent(MainActivity.this, LoginActivity.class);
                String intentUserName = userName.getText().toString();
                String intentPassword= pass.getText().toString();
                gotoLoginActivity.putExtra("username",intentUserName);
                gotoLoginActivity.putExtra("password",intentPassword);
                startActivity(gotoLoginActivity);

            }
        });
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }

            
        });

        Intent fromLoginIntent=getIntent();
        String userNameFromLogin=fromLoginIntent.getStringExtra("username");
        String passFromLogin=fromLoginIntent.getStringExtra("password");

        userName.setText(userNameFromLogin);
        userName.setText(passFromLogin);
    }

    private int register() {


        String password=pass.getText().toString();
        String passwordConfirm=confirmPass.getText().toString();

        if(userName.getText().toString().equals(""))
        {
            Toast.makeText(this, "Please Type A User Name", Toast.LENGTH_SHORT).show();
            userName.requestFocus();
            return 0;
        }
        if(email.getText().toString().equals(""))
        {
            Toast.makeText(this, "Please Type An Email", Toast.LENGTH_SHORT).show();
            email.requestFocus();
            return 0;
        }
        if(password.length()<8)
        {
            Toast.makeText(this, "Password Must Be More Than 7 Character", Toast.LENGTH_SHORT).show();
            pass.requestFocus();
            return 0;
        }

        if(!password.equals(passwordConfirm))
        {
            Toast.makeText(this, "Password Mismatch", Toast.LENGTH_SHORT).show();
            confirmPass.requestFocus();
            return 0;
        }

        if(saveDB())
        {
            Toast.makeText(this, "Registration Successfull", Toast.LENGTH_SHORT).show();
            Intent gotoLoginActivity = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(gotoLoginActivity);
        }
        else
        {
            Toast.makeText(this, "Registration Failed", Toast.LENGTH_SHORT).show();
        }
        return 1;
    }

    private boolean saveDB() {
        SharedPreferences pref=getApplicationContext().getSharedPreferences(PREF_NAME,0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(USER_NAME_PREF,userName.getText().toString());
        editor.putString(PASS_PREF,pass.getText().toString());
        editor.putString(EMAIL_PREF,email.getText().toString());
        return editor.commit();
    }
}
